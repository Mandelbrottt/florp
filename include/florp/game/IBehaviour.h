#pragma once
#include <memory>
#include <vector>
#include <entt.hpp>

namespace florp {
	namespace game {

		struct BehaviourBinding;
		
		/*
		 * Represents a behaviour that can be tied to a single GameObject
		 */
		class IBehaviour {
		public:
			bool    Enabled = true;
			virtual ~IBehaviour() = default;
			
			virtual void OnLoad(entt::entity entity){}
			virtual void OnUnload(entt::entity entity){}
			virtual void Update(entt::entity entity){}
			virtual void FixedUpdate(entt::entity entity){}
			virtual void LateUpdate(entt::entity entity){}
			virtual void RenderGUI(entt::entity entity) {}
			
		protected:
			IBehaviour() = default;
		};

		/*
		 * The component added to an entt entity to connect a behaviour to a specific entity
		 */
		struct BehaviourBinding {
			std::vector<std::shared_ptr<IBehaviour>> Behaviours;

			template <typename T, typename ... TArgs, typename = typename std::enable_if<std::is_base_of<IBehaviour, T>::value>::type>
			static void Bind(entt::registry& registry, entt::entity entity, TArgs&&... args) {
				BehaviourBinding& binding = registry.get_or_assign<BehaviourBinding>(entity);
				const std::shared_ptr<IBehaviour> behaviour = std::make_shared<T>(std::forward<TArgs>(args)...);
				binding.Behaviours.push_back(behaviour);
				behaviour->OnLoad(entity);
			}
		};
				
	}
}
